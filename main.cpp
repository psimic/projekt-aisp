#include <algorithm>
#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <unordered_map>
#include <vector>

class fraudTest {
private:
  std::unordered_map<double, std::vector<std::vector<double>>> data;

public:
  fraudTest(const std::string &filename) {
    std::ifstream file(filename);
    std::string line;

    while (std::getline(file, line)) {
      std::vector<double> row;
      std::stringstream ss(line);
      std::string value;

      while (std::getline(ss, value, ',')) {
        row.push_back(std::strtod(value.c_str(), nullptr));
      }

      double key = row[0];
      data[key].push_back(row);
    }

    file.close();
  }

  std::vector<std::vector<double>> search(const double key) {
    if (data.find(key) != data.end()) {
      return data[key];
    }

    return std::vector<std::vector<double>>();
  }

  std::vector<std::vector<double>> searchN(const double key, const int n) {
    std::vector<std::vector<double>> result;

    if (data.find(key) != data.end()) {
      std::vector<std::vector<double>> &rows = data[key];
      int count = std::min(n, static_cast<int>(rows.size()));

      for (int i = 0; i < count; i++) {
        result.push_back(rows[i]);
      }
    }

    return result;
  }

  void remove(const double key) { data.erase(key); }

  void removeN(const double key, const int n) {
    if (data.find(key) != data.end()) {
      std::vector<std::vector<double>> &rows = data[key];
      int count = std::min(n, static_cast<int>(rows.size()));
      rows.erase(rows.begin(), rows.begin() + count);
    }
  }

  double getMinValue(const int column) {
    double minValue = std::numeric_limits<double>::max();

    for (const auto &pair : data) {
      for (const auto &row : pair.second) {
        minValue = std::min(minValue, row[column]);
      }
    }

    return minValue;
  }

  double getMaxValue(const int column) {
    double maxValue = std::numeric_limits<double>::lowest();

    for (const auto &pair : data) {
      for (const auto &row : pair.second) {
        maxValue = std::max(maxValue, row[column]);
      }
    }

    return maxValue;
  }

  std::vector<std::vector<double>> getTopN(const int column, const int n) {
    std::vector<std::vector<double>> result;
    std::vector<double> columnData;

    for (const auto &pair : data) {
      for (const auto &row : pair.second) {
        columnData.push_back(row[column]);
      }
    }

    std::partial_sort(columnData.begin(), columnData.begin() + n,
                      columnData.end(), std::greater<double>());

    for (const auto &pair : data) {
      for (const auto &row : pair.second) {
        if (std::find(columnData.begin(), columnData.begin() + n,
                      row[column]) != columnData.begin() + n) {
          result.push_back(row);
        }
      }
    }

    return result;
  }

  void add(const std::vector<double> &row) {
    double key = row[0];
    data[key].push_back(row);
  }

  void addN(const std::vector<std::vector<double>> &rows) {
    for (const auto &row : rows) {
      double key = row[0];
      data[key].push_back(row);
    }
  }
};

int main() {
  fraudTest handler("fraudTest.csv");

  // Test search method
  double key = 42.0;
  auto start = std::chrono::steady_clock::now();
  std::vector<std::vector<double>> result = handler.search(key);
  auto end = std::chrono::steady_clock::now();
  std::cout << "Search time for key " << key << ": " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
            << " milliseconds\n";
   // Test searchN method
  int n = 5;
  start = std::chrono::steady_clock::now();
  std::vector<std::vector<double>> results = handler.searchN(key, n);
  end = std::chrono::steady_clock::now();
  std::cout << "SearchN time for key " << key << ": " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
            << " milliseconds\n";
  // Test remove method
  start = std::chrono::steady_clock::now();
  handler.remove(key);
  end = std::chrono::steady_clock::now();
  std::cout << "Remove time for key " << key << ": " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
            << " milliseconds\n";
  // Test removeN method
  start = std::chrono::steady_clock::now();
  handler.removeN(key, n);
  end = std::chrono::steady_clock::now();
  std::cout << "RemoveN time for key " << key << ": " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
            << " milliseconds\n";
   // Test getMinValue method
  int column = 2;
  start = std::chrono::steady_clock::now();
  double minValue = handler.getMinValue(column);
  end = std::chrono::steady_clock::now();
  std::cout << "getMinValue time for column " << column << ": " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
            << " milliseconds\n";
  // Test getMaxValue method
  start = std::chrono::steady_clock::now();
  double maxValue = handler.getMaxValue(column);
  end = std::chrono::steady_clock::now();
  std::cout << "getMaxValue time for column " << column << ": " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
            << " milliseconds\n";
   // Test getTopN method
  start = std::chrono::steady_clock::now();
  std::vector<std::vector<double>> topN = handler.getTopN(column, n);
  end = std::chrono::steady_clock::now();
  std::cout << "getTopN time for column " << column << ": " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
            << " milliseconds\n";
  // Test add method
  std::vector<double> newRow = {1.0, 2.0, 3.0};
  start = std::chrono::steady_clock::now();
  handler.add(newRow);
  end = std::chrono::steady_clock::now();
  std::cout << "add time: " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
            << " milliseconds\n";
  // Test addN method
  std::vector<std::vector<double>> newRows = {{4.0, 5.0, 6.0}, {7.0, 8.0, 9.0}};
  start = std::chrono::steady_clock::now();
  handler.addN(newRows);
  end = std::chrono::steady_clock::now();
  std::cout << "addN time: " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
            << " milliseconds\n";
}