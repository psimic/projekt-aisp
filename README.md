Trajanje izvrsavanja testova:

1. Search metoda : 0 ms
2. SearchN metoda : 0 ms
3. Remove metoda : 0 ms
4. RemoveN metoda : 0 ms
5. GetMinValue : 115 ms
6. GetMaxValue : 115 ms
7. GetTopN : 285 ms
8. Add : 0 ms
9. AddN : 0 ms

Search metoda: 
Metoda search ima zadatak da pronađe sve zapise koji se nalaze pod određenim ključem.
Prvo provjerava da li ključ postoji u mapi "data" koristeći funkciju find. Ako pronađe ključ, vraća vrijednost iz mape za taj ključ, što je vektor vektora double vrijednosti koji odgovaraju pronađenim zapisima.
Ako ključ nije pronađen u mapi, vraća prazan vektor, što znači da nema podataka koji odgovaraju datom ključu.

SearchN metoda: 
Metoda searchN ima istu funkcionalnost kao i search metoda samo sto searchN metoda pronalazi prvih n zapisa koji se nalaze pod određenim ključem.

Remove metoda:
Metoda remove ima zadatak da ukloni sve zapise koji se nalaze pod određenim ključem iz mape podataka "data".
Prvo provjerava da li ključ postoji u mapi "data" koristeći funkciju find. Ako pronađe ključ, koristi funkciju erase da ukloni taj ključ iz mape, čime se brišu svi zapisi koji su bili vezani za taj ključ.
Ako ključ nije pronađen u mapi metoda ne radi ništa, jer nema zapisa za uklanjanje pod datim ključem.

RemoveN metoda: 
RemoveN metoda radi istu stvar kao i remove metoda samo sto uklanja n zapisa.

GetMinValue metoda: 
Metoda getMinValue ima zadatak da pronađe najmanju vrijednost u određenoj koloni unutar svih zapisa u mapi podataka "data".
Prvo inicijalizira promjenjivu minValue na vrijednost koja predstavlja maksimalnu moguću vrijednost tipa double kako bi se osiguralo da će prva vrijednost koja se nađe biti manja od te vrijednosti.
Zatim prolazi kroz sve zapise u mapi "data", a zatim prolazi kroz sve vrijednosti u svakom zapisu. Za svaku vrijednost u odabranoj koloni, uspoređuje je sa trenutnom vrijednošću minValue i ažurira minValue ako je nova vrijednost manja.
Na kraju vraća minValue koja će biti najmanja vrijednost pronađena u odabranoj koloni u svim zapisima.

GetMaxValue metoda: 
Metoda getMaxValue ima zadatak pronaći najveću vrijednost u odabranoj koloni unutar svih zapisa u mapi podataka data.
Prvo inicijalizira se varijabla maxValue na vrijednost koja predstavlja minimalnu moguću vrijednost tipa double kako bi se osiguralo da će prva vrijednost koja se pronađe biti veća od te vrijednosti.
Zatim prolazi se kroz sve zapise u mapi "data", te unutar svakog zapisa prolazi se kroz sve vrijednosti. Za svaku vrijednost u odabranoj koloni, uspoređuje se s trenutnom vrijednošću maxValue i ažurira se maxValue ako je nova vrijednost veća.
Na kraju, metoda vraća maxValue, koja će biti najveća vrijednost pronađena u odabranoj koloni u svim zapisima.

GetTopN metoda: 
Metoda getTopN radi tako što prvo prolazi kroz sve podatke i gradi vektor columnData, koji sadrži vrijednosti izabrane kolone. Zatim koristi std::partial_sort da sortira prvih n elemenata tog vektora od najvećeg prema najmanjem.
Nakon sortiranja metoda ponovo prolazi kroz sve podatke i dodaje one čije se vrijednosti u odabranoj koloni nalaze među prvih n u sortiranom vektoru u result vektor.
Konačno, vraća result vektor koji sadrži prvih n redova sa najvećim vrijednostima u odabranoj koloni.

Add metoda:
Metoda add služi za dodavanje novog zapisa u mapu podataka "data".
Kada se pozove ova metoda prima referencu na vektor koji predstavlja novi zapis koji treba dodati. Prvi element u tom zapisu koristi se kao ključ za mapiranje u mapi "data". Nakon što se ključ odredi novi zapis se dodaje na kraj vektora koji pripada tom ključu.
Na primjer  ako pozovemo add sa vektorom [1.0, 2.0, 3.0], taj zapis će biti dodan na kraj vektora koji pripada ključu 1.0 u mapi data.
Ova metoda omogućava dinamičko dodavanje novih zapisa u postojeće podatke.

AddN metoda: 
Metoda sluzi za dodavanje n novih zapisa u mapu "data".

Koristio sam vektore, mape, ključeve u ovome projektu.
Koristio sam vektore jer se vektori koriste za čuvanje redova podataka. Svaki red podataka predstavljen je kao vektor brojeva tipa double. Vektori omogućavaju jednostavan pristup, iteriranje i dodavanje novih redova podataka. 

Mape se koriste za mapiranje ključeva na vektore redova podataka. Svaki ključ mapira se na vektor koji sadrži sve redove podataka povezane s tim ključem. Korištenjem mapa omogućava se brz pristup i manipulacija podacima prema ključu, bez potrebe za pretragom cjelog skupa podataka.

Ključevi se koriste kao identifikatori za različite skupove redova podataka. U ovom slučaju, ključevi su vrijednosti tipa double koje se koriste za mapiranje na odgovarajuće redove podataka. To omogućava brz i efikasan pristup podacima prema ključu.